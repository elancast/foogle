next_url = "/search.html";

function displayUser(user) {
    alert(user);
    alert('wooo');
}

function go2() {
    var accessToken = window.location.hash.substring(1);
    var path = "https://graph.facebook.com/me/home?";
    var queryParams = [accessToken, 'callback=displayUser'];
    var query = queryParams.join('&');
    var url = path + query;

    // use jsonp to call the graph
    var script = document.createElement('script');
    script.src = url;
    document.body.appendChild(script);
}

function getNextUrl() {
    var url = window.location.href;
    var oldPath = window.location.pathname;
    var newPath = oldPath.indexOf("index") >= 0 ?
	oldPath.replace("index", "search") : 
	oldPath + "search.html";
    return url.replace(oldPath, newPath);
}

function go() {
    doIt();
}

function doIt() {
    var appID = 150854508344687;
    var path = 'https://www.facebook.com/dialog/oauth?';
    var queryParams = ['client_id=' + appID,
		       'redirect_uri=' + getNextUrl(),
		       'response_type=token',
		       'scope=read_stream,friends_likes,user_status,friends_status'];
    var query = queryParams.join('&');
    var url = path + query;
    window.open(url, '_self');
}
