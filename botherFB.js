var post = '\
    <li class=g><div class=vsc><h3 class="r"><a href="{0}" class=l>{1}</a></h3>\
    <div class="s"><div class="f kv">\
<cite onclick="alert(\'hi!\');"><span style="float:left;">{2}&nbsp;&nbsp;</span>\
<img onclick="javascript:this.src=\'./images/plus1.png\';" src="./images/plus2.png" style="height:15px; width:24px; float:left;position:relative;" />\
<span onclick="alert(\'hi!\');" ><iframe onclick="alert(\'hi!\');" src="//www.facebook.com/plugins/like.php?href={4}&amp;send=false&amp;layout=standard&amp;width=24&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=15&amp;appId=150854508344687" scrolling="no" frameborder="0" style="display:inline; border:none; overflow:hidden; width:24px; height:15px; position:relative; float:left; opacity:.1; left:-25px; " allowTransparency="true"></iframe>\
</span></cite><br></div><span class=st>\
{3}<b></b><br></span></div></div>';

var graph_url = "https://graph.facebook.com/me/home?";
var glimit = 50;
var since = "since=";
var goffset = 0;

String.prototype.format = function() {
    var formatted = this;
    for(arg in arguments) {
        formatted = formatted.replace("{" + arg + "}", arguments[arg]);
    }
    return formatted;
}

function getAccessId(prequel) {
    var params = window.location.hash.substring(1).split("&");
    var result = "";
    for (var i = 0; i < params.length; i++) {
	if (params[i].indexOf("access_token") == 0 ||
	    params[i].indexOf("expires_in") == 0) {
	    if (result.length != 0) result += "&";
	    result += params[i];
	}
    }
    return prequel + result;
}

function getStats(news) {
    var likes = news.likes == null ? 0 : news.likes.count;
    var comments = news.comments == null ? 0 : news.comments.count;
    return "Comments: " + comments + ", likes: " + likes;
}

function getUrl() {
    var ret = new Array();
    var sinceValue = null;

    // Get the URL
    var url = graph_url + "limit=" + glimit + getAccessId("&");
    if (sinceValue != null) {
	url += since + sinceValue;
    }
    url += "&offset=" + goffset + "&callback=newsFeedCallback";
    goffset += glimit;

    return url;
}

function getPicLink(news) {
    return news.picture.replace("_s.jpg", "_n.jpg");
}

function photoize(news, arr) {
    var pic = getPicLink(news);
    // change 1 if want to change the timestamp likes/comms
    var a = arr[1].indexOf('0000') == -1 ? arr[1] :
	arr[1].split('000, ')[1];
    return [ pic, arr[3], arr[0], a, arr[3], arr[2], 0];
}

function parsePhoto(news) {
    var pic = news.picture.replace("_s.jpg", "_n.jpg");
    return [ getPicLink(news), news.link, news.from.name +
	     " posted a picture", news.name, parseTime( news.created_time ),
	     news.message == null ? "" : news.message ];
}

/*
 * Title: Bla wrote a status | Bla wrote on Bla's wall
 * Descr: Likes: %d, comments: %d
 * Content: content
 * Link: link
 */
function parseStatus(news) {
    // Verify sanity...
    var arr = [null, null, null, null];
    if (news.from == null || news.from.name == null) {
	//	alert("status from is null");
	return null;
    } else if (news.message == null && news.story == null) {
	//	alert("no story for news story");
	return null;
    }

    // Fill
    var actor = news.from.name;
    var actee = news.to != null ? news.to.data[0].name : null;
    arr[0] = (actee != null ?
	      actor + " posted on " + actee + "'s wall" :
	      actor + " posted a status update");
    arr[1] = parseTime(news.created_time) + ", " + getStats(news);
    arr[2] = (news.message == null ? news.story : news.message);
    arr[3] = news.actions != null ? news.actions[0].link : "";
    return arr;
}

/*
 * types: new friends, prof pic change, link posted
 * Title: Bla got new friends! | Link
 * Descr: likes / comments | Caption
 * Content: the new friends | Message
 * Link
 */
function parseLink(news) {
    var arr = [null, null, null, null];

    // Link link link link
    if (news.caption != null) {
	arr[0] = news.from.name + " posted a link: " + news.link;
	var from = news.from != null ? news.from.name + " said: " :null;
	arr[2] = news.message == null || news.message.length == 0 ?
	    news.caption : ((from == null ? "" : from) + news.message);
	arr[2] = news.description == null ? arr[2] :
	    news.name + ": " + news.description + "<br>" + arr[2];
    } else if (news.story != null) {
	arr[0] = news.story;
	arr[2] = "";
	if (news.story_tags != null) {
	    for (var prop in news.story_tags) {
		var strs = news.story_tags[prop];
		for (var i = 0; i < strs.length; i++) {
		    arr[2] += ", " + strs[i].name;
		}
	    }
	    arr[2] = arr[2].substring(2);
	}
    } else if (news.name == null) {
	return null;
    } else {
	arr[0] = news.from.name + " posted: " + news.name;
	arr[2] = news.message;
    }

    arr[1] = parseTime(news.created_time) + ", " + getStats(news);
    arr[3] = news.link;
    return arr;
}

function parseStory(news) {
    var arr = [null, null, null, null];
    var a = news.story.indexOf("likes");
    var b = news.story.indexOf("link");
    if (news.story == null || news.story.indexOf("likes") == -1 ||
	news.story.indexOf("link") == -1) {
	return null;
    }
    arr[0] = news.story;
    arr[2] = "";
    if (news.name != null || news.description != null) {
	arr[2] = news.name == null ? "" : news.name;
	arr[2] = news.description == null ? arr[2] :
	    arr[2] + ": " + news.description;
    } else if (news.story_tags != null) {
	for (var prop in news.story_tags) {
	    var strs = news.story_tags[prop];
	    for (var i = 0; i < strs.length; i++) {
		arr[2] += ", " + strs[i].name;
	    }
	}
	arr[2] = arr[2].substring(2);
    }

    var likes = news.likes == null ? 0 : news.likes.count;
    var comments = news.comments == null ? 0 : news.comments.count;
    arr[1] = "Likes: " + likes + ", comments: " + comments;
    arr[3] = news.link;
    return arr;
}

function processNewsFeedItem(news) {
    var created = parseTime(news.created_time);
    var type = news.type;
    var title = null, descr = null, content = null;
    var ret = null;
    var photo = false;
    if (news.story != null) {
	ret = parseStory(news);
    } else if (type == "link") {
	ret = parseLink(news);
    } else if (type == "status") {
	ret = parseStatus(news);
    } else if (type == "photo") {
	ret = parsePhoto(news);
	photo = true;
    }
    if (ret == null) return "";
    if (news.picture != null && type != "photo") {
	ret = photoize(news, ret);
	photo = true;
    }
    if (photo) return ret;
    else return post.format(ret[3], ret[0], ret[1], ret[2], ret[3]);
}

var photoPost = '\
    <tr><td colspan="2" valign="top" width="737"><!--m--><div class="vresult">\
    <div class="vsc vsgv" sig="e3x"><table class="ts" style="width:auto"><tbody>\
    <tr><td valign="top" style="padding:7px 8px 0 0"><a id="v14105995976581728904" \
href="{0}" style="text-decoration:none"><div style="position:relative">\
    <img src="{0}" align="middle" border="1" height="60" id="vidthumb6" \
class="th vidthumb6" style="margin:0px 0px 0px 0px" width="80"><div \
style="margin-top:-23px;margin-right:4px;text-align:right"><img \
src="images/play_c.gif" alt="" border="0" height="20" \
style="-moz-opacity:.88;filter:alpha(opacity=88);opacity:.88" width="20"></div>\
</div></a></td><td valign="top" style="padding:5px 10px 0 0"><a href="{1}" \
class="l">{2}</a> - <span><cite>{3}</cite></span><br><span class="f"><span \
class="nobr">{4}</span></span><br><div style="margin:4px 0">{5}</div></td>\
</tr></tbody></table></div></div><!--n--></td></tr>';

var photoBgn = '\
    <li class="g videobox" id="videobox"><h3 class="r inl"><a href="">\
{0}</a></h3><table class="ts" style="margin-top:2px"><tbody>\
';

var photoEnd = '</tbody></table><!--m--></li>';

function cutoff(s) {
    var a = s.substring(0, 50);
    if (a.length != s.length) a += "...";
    return a;
}

function injectPhotoInfo(arr) {
    var photoStr = photoBgn.format("Videos for <b>hello world</b>");
    var newsStr = photoBgn.format("News for <b>hello world</b>");
    var goodPhoto = false, goodNews = false;
    for (var i = 0; i < arr.length; i++) {
        var s = photoPost.format(arr[i][0],arr[i][1],cutoff(arr[i][2]),
				 arr[i][3],cutoff(arr[i][4]),arr[i][5]);
	s = s.format(arr[i][0]);
	if (arr[i].length == 6) {
	    photoStr += s; goodPhoto = true;
	} else {
	    newsStr += s; goodNews = true;
	}
    }
    photoStr += photoEnd; newsStr += photoEnd;
    return [goodPhoto ? photoStr : null, goodNews ? newsStr : null];
}

function newsFeedCallback(response) {
    if (response == null) {
	alert("ERROR OH SHIT OH NO PLEASE NO AAAAAAA newsnull");
	return "ERROR";
    }

    var arr = response.data;
    var texts = new Array();
    var pics = new Array();
    for (var i = 0; i < arr.length; i++) {
	var ret = processNewsFeedItem(arr[i]);
	if (ret == null || ret.length == 0) continue;
	else if (ret instanceof Array) pics.push(ret);
	else texts.push(ret);
    }

    var whoo = '';
    pics = injectPhotoInfo(pics);
    var last = texts.length-1 < 10 ? texts.length-1 : 10;
    for (var i = 0; i < texts.length; i++) {
	if (i == 3 && pics[0] != null) whoo += pics[0];
	if (i == last && pics[1] != null) whoo += pics[1];
	whoo += texts[i];
    }
    nextLink = response.paging.next;
    prevLink = response.paging.previous;

    if (addToPage) {
	putHtml(whoo);
	addToPage = false;
	add_content("preFetchNext");
    }
}

function preFetchNext(response) {
    nextPage = response;
}

function putHtml(html) {
    document.getElementById("hbdR").innerHTML = html;
}

function add_content(callback) {
    var url = nextLink == null ? getUrl() : nextLink;
    if (url == null) {
	alert("ERROR OH SHIT OH NO PLEASE NO AAAAAAA urlnull");
	return;
    }
    if (callback != null) {
	if (url.indexOf("newsFeedCallback") >= 0) {
	    url = url.replace("newsFeedCallback", callback);
	} else if (url.indexOf("preFetchNext") >= 0) {
	    url = url.replace("preFetchNext", callback);
	}
    }

    var script = document.createElement('script');
    script.src = url;
    document.body.appendChild(script);
}

function changePage() {
    page ++;
    document.getElementById("PUTMEHERE").innerHTML = page;
}

var nextLink = null;
var nextPage = null;
var addToPage = true;
var loading = "<img src='./images/loading.gif'/>";
var page = 1;
function getNext() {
    changePage();
    scroll(0, 0);
    putHtml(loading);
    if (nextPage != null) {
	addToPage = true;
	newsFeedCallback(nextPage);
	addToPage = false;
	nextPage = null;
    } else {
	addToPage = true;
	add_content("newsFeedCallback");
    }
}

function my_assert(cond)
{
    return;

    if (cond) ;
    else alert("ASSERT FALSE!!");
}

function parseTime(timeStamp, timeZone) 
{
    timeZone = (typeof timeZone == "undefined") ? -4 : optionalArg;
    
    
    // CONSTANTS
    
    var formula = "year-mo-dy|hr:mn:sc+0000";
    var months = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                       "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var numDaysPer = [31, 31, 28, 31, 30, 31, 30,
                          31, 31, 30, 31, 30, 31];
    
    // VALIDATION
    
    my_assert( timeStamp.length == timeZone.length );
    
    var index = [  4,   7,  10,  12,  14,  16,  17,  18,  19,  20];
    var char  = ['-', '-', 'T', ':', ':', '+', '0', '0', '0', '0'];
    for (var i = 0; i < index.length; i++)
        my_assert( timeStamp.charAt(index[i]) == char[i] );
    
    // EXTRACTION
    
    var yr = parseInt( timeStamp.substr(formula.indexOf("year"), 4), 10);
    var mo = parseInt( timeStamp.substr(formula.indexOf("mo"), 2), 10);
    var dy = parseInt( timeStamp.substr(formula.indexOf("dy"), 2), 10);
    
    var hr = parseInt( timeStamp.substr(formula.indexOf("hr"), 2), 10);
    var mn = parseInt( timeStamp.substr(formula.indexOf("mn"), 2), 10);
    var sc = parseInt( timeStamp.substr(formula.indexOf("sc"), 2), 10);
    
    // TIME ZONE CORRECTION
    
    hr += timeZone;
    
    if (hr < 0) { dy--; hr += 24; }
    if (dy < 1) { mo--; dy = numDaysPer[mo]; }
    if (mo < 1) { yr--; mo += 12; }
    
    if (hr > 23) { dy++; hr -= 24; }
    if (dy > numDaysPer[mo]) { mo++; dy = 1; }
    if (mo > 12) { yr++; mo -= 12; }
    
    // CONVERT AND RETURN
    // eg. Oct 16, 2011 -- 12:33 AM
    
    ret = "";
    
    if (hr == 0)           ret += (hr + 12);
    if (hr > 0 && hr < 13) ret += hr;
    if (hr > 12 )          ret += (hr - 12);
    
    ret += ":" + (mn < 10 ? "0" : "") + mn + " ";
    ret += (hr < 12 ? "AM" : "PM") + " ";
    
    ret += months[mo] + " ";
    ret += dy;
    
    return ret;
}



